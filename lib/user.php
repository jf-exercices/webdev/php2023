<?php

/**
 * @param string $field
 * @param string $value
 * @return mixed
 */
function getUser(string $field, string $value): mixed
{

    if (!in_array($field, getColumns('user'))) {
        return false;
    }

    $connect = connect();

    // 2. QUERY
    $request = $connect->prepare("SELECT * FROM user WHERE $field = ?");

    $params = [
        trim($value),
    ];

    // 3. EXECUTE
    $request->execute($params);

    // 4. FETCH
    return $request->fetchObject();
}

/**
 * @param string $field
 * @param string $value
 * @return bool
 */
function userExists(string $field, string $value): bool
{
    if (is_object(getUser($field, $value))) {
        return true;
    } else {
        return false;
    }
}

/**
 * @param int $userid
 * @return string
 */
function getUserPhoto(int $userid): string
{
    $imagepath = 'image/profile/' . $userid . '/' . $userid;
    if (is_array(IMG_EXT)) {
        foreach (IMG_EXT as $extension) {
            $filename = $imagepath . '.' . $extension;
            if (file_exists($filename)) {
                return $filename;
            }
        }
    }
    return '';
}

/**
 * @return void
 */
function logout(): void
{
    session_unset();
    session_destroy();
    session_write_close();
    header('Location: index.php');
    die;
}