<?php

/**
 * @param string $content
 * @return void
 */
function getContent(string $content): void
{
    if (is_array(FILE_EXT)) {
        foreach (FILE_EXT as $extension) {
            $filename = $content . '.' . $extension;
            if (file_exists($filename)) {
                include_once $filename;
            }
        }
    }
}

/**
 * @param int|null $country
 * @return string
 */
function getCountry(int|null $country): string
{
    if ($country == 1) {
        return 'Allemagne';
    } elseif ($country == 2) {
        return 'Belgique';
    } elseif ($country == 3) {
        return 'France';
    } else {
        return 'Aucun';
    }
}

/**
 * @param int $status
 * @return string
 */
function getStatus(int $status): string
{
    if ($status == 1) {
        return 'Actif';
    } else {
        return 'Inactif';
    }
}

/**
 * @param object|array $options
 * @param string $selected
 * @return string
 */
function getFormOptions(object|array $options, string $selected = ''): string
{
    $output = '';
    foreach ($options as $key => $value) {
        if ($key == $selected) {
            $attr = 'selected';
        } else {
            $attr = '';
        }
        $output .= '<option value="' . $key . '" ' . $attr . '>' . $value . '</option>';
    }
    return $output;
}