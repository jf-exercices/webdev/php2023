<?php

include_once 'check.php';

if (!empty($_POST['login']) && !empty($_POST['pwd'])) {

    // Vérifier si l'utilisateur, dont le login du formulaire correspond au login en DB, existe
    $login = trim($_POST['login']);

    $user = getUser('login', $_POST['login']);

    // Utilisation de la fonction native PHP password_verify pour valider un mot de passe et sa valeur de hashage stockée en DB par la fonction native password_hash
    if (password_verify($_POST['pwd'], $user->pwd)) {
        // L'id de l'utilisateur est stocké en session
        if(!empty($user->id)) {
            $_SESSION['userid'] = $user->id;
            // Si le mot de passe est vérifié, on met à jour le champ "lastlogin" dans la table user en DB
            $sql = "UPDATE user SET lastlogin = NOW() WHERE id = ?";
            // QUERY
            $connect = connect();
            $update = $connect->prepare($sql);
            // EXECUTE
            $update->execute([$user->id]);

            if ($update->rowCount()) {
                echo 'Dernière précédente connexion : ' . $user->lastlogin;
            }
        }
        $url = 'index.php?view=view/profile';

    } else {
        $_SESSION['alert'] = 'Connexion échouée';
        $url = 'index.php?view=view/login';
    }
    // redirect
    header('Location: ' . $url);

}
