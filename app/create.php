<?php

include_once 'check.php';

// on détermine l'url de redirection par défaut (le formulaire de création)
$url = 'index.php?view=view/create';

if (!empty($_POST['login']) && !empty($_POST['pwd']) && filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {

    // on vérifie si un utilisateur existe déjà avec le login fourni dans le formulaire
    // pour ce faire, nous utilisons la fonction userExists créée dans lib/user.php
    if (userExists('login', $_POST['login'])) {
        $_SESSION['alert'] = 'L\'utilisateur existe déjà!';
        header('Location: ' . $url);
        die;
    }

    // initialialisation des variables de requête SQL, en cas de paramètres facultatifs (country dans notre cas)
    $sqlField = '';
    $sqlValues = '';

    // version basique
    $login = $_POST['login'];
    $pwd = $_POST['pwd'];
    $email = $_POST['email'];
    $country = $_POST['country'];

    // version alternative : optimisé et évolutif
    foreach ($_POST as $key => $value) {
        $$key = $value;
    }

    // on détermine les paramètres à passer à la requête SQL PDO
    $params = [
        trim($login),
        password_hash($pwd, PASSWORD_DEFAULT),
        $email
    ];

    // si le pays est sélectionné dans le formulaire, on modifie la requête SQL
    if (!empty($country)) {
        $sqlField = ', country';
        $sqlValues = ', ?';
        $params[] = $country;
    }

    // 1. connexion
    global $connect;
    // => $connect = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST . ';charset=utf8', DB_USER, DB_PASSWORD);
    // alternative
    $connect = connect();

    // 2. QUERY
    $insert = $connect->prepare("INSERT INTO user (login, pwd, email, created $sqlField) VALUES (?, ?, ?, NOW() $sqlValues)");

    // 3. EXECUTE
    $insert->execute($params);

    if ($insert->rowcount()) {
        // on récupère l'id de la dernière ligne insérée en DB
        $userid = $connect->lastInsertId();
        // on stocke en session le message de réussite, pour l'afficher sur la page de redirection (via index.php)
        $_SESSION['alert'] = 'Utilisateur ' . $login . ' a été créé avec succès';
        $_SESSION['alert-color'] = 'success';
        // on modifie l'url de redirection (formulaire de login)
        $url = 'index.php?view=view/login';

        // gestion de la photo de profil
        if ($_FILES['photo']['tmp_name'] &&
            $_FILES['photo']['name']
        ) {
            if ($_FILES['photo']['type'] == 'image/png' || $_FILES['photo']['type'] == 'image/jpeg') {
                if ($_FILES['photo']['size'] <= 1024000) {

                    // copie de l'image sur le serveur

                    // on détermine le dossier de destination
                    $imagepath = ROOT_PATH . '/image/profile/';
                    if (is_dir($imagepath . $userid) == false) {
                        // si le dossier spécifique à l'utilisateur n'existe pas, on le crée
                        mkdir($imagepath . $userid, 0755);
                    }
                    // on détermine le nom du fichier qui sera stocké sur le serveur (le même que le nom du client)
                    $basename = basename($_FILES['photo']['name']);
                    // alternative, on force le nom du fichier (par exemple avec l'id de l'utilisateur)
                    $basename = $userid . '.' . substr($_FILES['photo']['type'], 6, 4);
                    $basename = $userid . '.' . substr($_FILES['photo']['type'], strpos($_FILES['photo']['type'], '/') + 1, 4);
                    // on déplace le fichier temporaire (tmp_name) dans le dossier de destination
                    $move = move_uploaded_file($_FILES['photo']['tmp_name'], $imagepath . $userid . '/' . $basename);
                    if ($move) {
                        $_SESSION['alert'] .= '<br>L\'image de profil a été correctement envoyée !';
                    } else {
                        $_SESSION['alert'] .= '<br>L\'image de profil n\'a pas été correctement envoyée !';
                    }
                }
            } else {
                $_SESSION['alert'] .= '<br>L\'image doit être au format PNG ou JPG';
            }
        } else {
            $_SESSION['alert'] .= '<br>La photo de profil n\'a pas été envoyée vers le serveur';
        }
    } else {
        $_SESSION['alert'] = 'La création de l\'utilisateur a échoué';
    }
} else {
    $_SESSION['alert'] = 'La création a échoué';
}
header('Location: ' . $url);
die;