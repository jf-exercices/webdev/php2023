<?php

// Initialisation de la session ($_SESSION) dans index.php afin qu'il soit utilisé dans tous les scripts appelés via le paramètre 'view'
// Attention, la fonction native Php session_start() doit être appelée dans chaque script où la session sera utilisée
// Il est conseillé de nommer la session et de la limiter dans le temps
session_name('WEB' . date('Ymd'));
session_start(['cookie_lifetime' => 3600]);

require_once 'config.php';
require_once 'lib/pdo.php';
require_once 'lib/access.php';
require_once 'lib/output.php';
require_once 'lib/user.php';

$connect = connect();

include_once 'view/header.html';
include_once 'view/menu.php';

// alert message
if (!empty($_SESSION['alert'])) {
    if (!empty($_SESSION['alert-color'])
        && in_array($_SESSION['alert-color'], ['danger', 'info', 'success', 'warning']) // white-list
    ) {
        $alertColor = $_SESSION['alert-color'];
        unset($_SESSION['alert-color']);
    } else {
        $alertColor = 'danger';
    }
    echo '<div class="alert alert-' . $alertColor . '">' . $_SESSION['alert'] . '</div>';
    // only once
    unset($_SESSION['alert']);
}

// Vérifie la présence d'un paramètre 'view' dans l'URL
if (!empty($_GET['view'])) {
    getContent($_GET['view']);
}

include_once 'view/footer.html';