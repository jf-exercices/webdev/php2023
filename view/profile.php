<?php
$go_out = false;

if (!empty($_SESSION['userid'])) {

    $user = getUser('id', $_SESSION['userid']);
    if (!is_object($user)) {
        $go_out = true;
    } else {
        // manage photo
        $photo = getUserPhoto($user->id);
    }
} else {
    $go_out = true;
}

// redirection to login form if user not authenticated
if ($go_out) {
    header('Location: index.php?view=view/login');
    die;
}

$output = '<h2>Profil</h2>
<table class="table">
    <thead>
        <tr>
            <th>Intitulé</th>
            <th>Valeur</th>
        </tr>
    </thead>
    <tbody>';

if (!empty($user)) {
    foreach ($user as $key => $value) {
        if ($key == 'country') {
            $value = getCountry($value);
        } elseif ($key == 'status') {
            $value = getStatus($value);
        } elseif ($key == 'pwd' || $key == 'id') {
            continue;
        } elseif ($key == 'lastlogin' || $key == 'created') {
            $value = date_format( new DateTime($value),"d/m/Y H\hi");
        }
        $output .= '<tr><th>' . ucfirst($key) . '</th><td>' . $value . '</td></tr>';
    }
    if (!empty($photo)) {
        $output .= '<tr><th>Photo</th><td><img src="' . $photo . '" alt="photo de profil" class="d-block img-fluid"></td></tr>';
    }
}
$output .= '</tbody></table>';

echo $output;

include_once 'update.php';