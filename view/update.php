<?php

if (!empty($user)) {

    $countries = [
        0 => 'Aucun',
        1 => 'Allemagne',
        2 => 'Belgique',
        3 => 'France'
    ];

    if (empty($user->country)) {
        $user->country = 0;
    }

    $output = '<hr>
                 <h3><a href="#profile-update-collapse" data-bs-toggle="collapse" role="button" class="text-decoration-none">Update</a></h3>
                 <div class="collapse" id="profile-update-collapse">
                    <form action="index.php?view=app/update" method="post" enctype="multipart/form-data">
                        <input type="hidden" id="uu-userid" name="id" value="' . $user->id . '">
                        <label for="uu-email">Email</label>
                        <input type="email" id="uu-email" name="email" class="form-control" value="' . $user->email . '">
                        <label for="uu-lang">Pays</label>
                        <select name="country" id="uu-country" class="form-control">
                            ' . getFormOptions($countries, $user->country) . '
                        </select>
                        <label for="uu-photo">Photo</label>
                        <p><input type="file" id="uu-photo" name="photo"></p>
                        <input type="submit" class="btn btn-primary">
                    </form>
                 </div>';
    echo $output;
}
