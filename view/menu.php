<nav class="nav">
    <a href="index.php" class="nav-link">Accueil</a>
    <a href="index.php?view=view/profile" class="nav-link">Profil</a>
    <a href="index.php?view=view/create" class="nav-link">Create</a>
    <?php
    if (!empty($_SESSION['userid'])) {
        ?>
        <a href="index.php?view=view/logout" class="nav-link">Logout</a>
        <?php
    } else {
        ?>
        <a href="index.php?view=view/login" class="nav-link">Login</a>
        <?php
    }
    ?>
</nav>