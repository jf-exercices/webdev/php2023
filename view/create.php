<form action="index.php?view=app/create" method="post" enctype="multipart/form-data">
    <label for="login">Login</label>
    <input type="text" id="login" name="login"><br>
    <label for="email">Email</label>
    <input type="email" id="email" name="email"><br>
    <label for="pwd">Mot de passe</label>
    <input type="password" id="pwd" name="pwd"><br>
    <label for="photo">Photo de profil</label>
    <input type="file" id="photo" name="photo" accept="image/jpeg,image/png"><br>
    <label for="country">Pays</label>
    <select name="country" id="country">
        <option value="">Select...</option>
        <option value="1">Allemagne</option>
        <option value="2">Belgique</option>
        <option value="3">France</option>
    </select><br>
    <input type="submit">
</form>