<?php

// alternative
//include_once '../lib/access.php';

if (function_exists('checkUrl') && function_exists('logout')) {
    checkUrl();
    logout();
} else {
    header('Location: ../index.php');
    die;
}
